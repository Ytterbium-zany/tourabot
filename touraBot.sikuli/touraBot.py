#import
import java.awt.Toolkit 
import time
import sys
import shutil
import os
    
#######################################################
# CONFIGURATION
#######################################################
# setup required - True or False
isScreenshotRequired = False
isRepairRequired = True

# change to 1-1 to get sakura w/o changing team number and farming map -> too lazy bro!!
isEnergyCharging = False

# farmingMapIndex e.g. "4-4", "5-3", "6-2", "E-3", "OSAKA"
farmingMapIndex = "5-4"
isBossEnter = True

# formation during farming "ADV" = select advantage formation, other = "ATK", "DEF", "SPD"
formationType = "ADV"

# wound threshold as a number to stop farming e.g. 1 for yellow, 2 for orange, 3 for red
woundBeginThreshold = 3
woundEndThreshold = 3

# check exhaust
isCheckExhaust = True

# custom double click
isCustomDoubleClick = False

#######################################################
if len(sys.argv) > 1:
    farmingMapIndex = str(sys.argv[1])
if farmingMapIndex == "1-1":
    isEnergyCharging = True
if len(sys.argv) > 2:
    isBossEnter = "True" == str(sys.argv[2])
if len(sys.argv) > 3:
    formationType = str(sys.argv[3])
if len(sys.argv) > 4:
    isRepairRequired = "True" == str(sys.argv[2])
if len(sys.argv) > 5:
    isCustomDoubleClick = "True" == str(sys.argv[5])

#function
def beep():
    java.awt.Toolkit.getDefaultToolkit().beep()
    log("beep!!!")

def log(str):
    log = "[log] |" + time.strftime("%Y-%m-%d %H:%M:%S") + "| "
    print(log + str)

	
def takeScreenShot(type):
    App.focus("Chrome")
    focusWindow = App.focusedWindow()
    regionImage = capture(focusWindow)
    currentDir = sys.argv[0]
    name = type + '_' + time.strftime("%Y%m%d%H%M%S") + '.png'
    shutil.move(regionImage, os.path.join(currentDir+'/screen_shot', name))
    log("take screen shot: " + name)

def clickMvp():
    sleep(1)
    if isCustomDoubleClick :
        click("1614184220535.png")
        sleep(1)
        click("1614184220535.png")
    else :
        doubleClick("1614184220535.png")

def repair():
    sleep(5)
    needRepair = True
    log("Need Repair = " + str(needRepair))
    while needRepair:
        log(".")
        if exists("expedite.png", 0) != None:
            log("xxx")
            break
        elif exists("1466228790721.png", 0) != None:
            log("yyy")
            break
        elif exists("1559138867412.png", 5) != None:
            click("1559138867412.png")
            continue
    
        elif exists("1578551467618.png", 5) != None:
            repairBtn = findAll("1578551467618.png")
            sortRepairBtn = sorted(repairBtn, key=lambda m:m.y) 
            for btn in sortRepairBtn:
                log("click repair btn")
                swordRegion = btn.left(200)
                if swordRegion.exists("1474708212328.png", 5) != None:
                    continue
                click(btn)
                break
    
            if exists("1623842225046.png", 2) != None:
                click("1623842225046.png")
            if exists("1578551570724.png", 2) != None:
                click("1578551570724.png")
                needRepair = False
            continue
        elif exists("1559139040509.png", 0) == None:
            log("enter repairment")
            goToRepair()
            continue
    log("end of repairment...")
    sleep(5)
    goToHonmaru()

def checkGoHome():
    global haveToGoHome
    if not isBossEnter:
        if exists(stopNode1, 0)!=None:
            haveToGoHome = True
            log("boss is next =[]=")
            
    if woundEndThreshold <= 3 and exists(Pattern("wound_severe.png").similar(0.81), 0):
        haveToGoHome = True
        log("severe injury found")
    elif woundEndThreshold <= 2 and exists("wound_medium.png", 0):
        haveToGoHome = True
        log("medium injury found")
    elif woundEndThreshold <= 1 and exists("wound_small.png", 0):
        haveToGoHome = True
        log("small injury found")
            
    if haveToGoHome:
        log("go home")
        click("1559060085565.png")
        sleep(1)
        click("1577529715054.png")
    else:
        log("forwarding")
        click("1559060095585.png")
        wait(nodeDetect)
        
def goFarming():
    global haveToGoHome
    
    log("ikimasu!!")
    click("1575970282642.png")
    haveToGoHome = False
    if exists("1577529705357.png", 2):
        log("max level reach... =_=")
        if(isEnergyCharging):
            click("1577529715054.png")
        else:
            click("1577529729843.png")
            sleep(1)
            click("honmaru.png")
            sys.exit("Mata ne arujisama!")

    if exists("1611996252792.png"):
        log("Severe wound detected")
        click("1577529729843.png")
        click("1577529652016.png")
        goToHonmaru()
        repair()
        return
    isCheckExhaust = True
    wait(3)
    wait(nodeDetect)

def chooseFormation():
    log("choose formation")
    if formationType == "ADV":
        if exists("1559059260838.png", 0):
            click("1559059260838.png")
        else:
            click(defaultFormation)
    elif formationType == "DEFADV":
        if exists(Pattern("1597245855512.png").exact(),0):
            click(Pattern("1597245878117.png").similar(0.80))
        else:
            click(defaultFormation)
                        
    elif formationType == "SPDADV":
        if exists(Pattern("1559698306093.png").exact(),0):
            click(Pattern("1559059345731.png").similar(0.90))
        else:
            click(defaultFormation)
    else:
        click(defaultFormation)
    for j in range(0, 120):
        if not exists("1614184220535.png", 0):
            continue
        mvpChecking()
        break

def mvpChecking():
    log("mvp")
    sleep(1)
    clickMvp()
    if exists("1559060027510.png", 6):
        checkGoHome()
    elif exists("1559429547477.png"):
        log("honmaru e...")
    else:
        receiveNewMember()

def receiveNewMember():
    log("receive new member")
    App.focus("Chrome")
    focusWindow = App.focusedWindow()
    regionImage = capture(focusWindow)
    click(regionImage)

    if exists("1559060027510.png", 3):
        checkGoHome()

def goToHonmaru():
    click("1623756120603.png")
    click("1559061060355.png",1)

def goToSorting():
    click("1623756120603.png")
    click("1559060813834.png",1)

def goToTeamSetup():
    click("1623756120603.png")
    click("1559060917247.png",1)

def goToRepair():
    hover("1623756120603.png")
    click("1559060951389.png",1)

def logSetup():
    log("Farming map : " + farmingMapIndex)
    log("Enter boss  : " + str(isBossEnter))
    log("Formation   : " + formationType)
    log("Screenshot  : " + str(isScreenshotRequired))
    log("Repair      : " + str(isRepairRequired))
    log("Custom click: " + str(isCustomDoubleClick))

#######################################################
farmingEraIndex = farmingMapIndex[:1]

if isEnergyCharging:
    farmingEraIndex = "1"
    farmingMapIndex = "1-1"
    isBossEnter = True

logSetup()
setThrowException(False)

# stopNodeIndex e.g. "0" for non-stop mode, others are "3-4", "5-3"
if isBossEnter:
    stopNodeIndex = "0"
else:
    stopNodeIndex = farmingMapIndex

#param setup
continuePlaying = True
haveToGoHome = False
farmingEraDict = {
        "1": "1586189203837.png", "3": "1586258749857.png",  "4": "1586189231725.png",
        "5": "1586189252684.png", "6": "1586189266660.png", "7": "1586189303939.png", "8":"1586189316820.png"}
farmingMapDict = {
        "1-1": "1586189443715.png", "4-2": "1586189426710.png",
        "5-4": "1624150852018.png", "6-1": "1586189388129.png", "6-2": "1586189396366.png", 
        "7-1": "1586189360403.png", "7-3": "1586189370125.png",
        "8-1":"1586189334458.png", "8-2":"1586189343201.png"}
stopNodeDict = {
        "4-2": Pattern("1583057262884.png").similar(0.98), "5-3": "1474995369935.png", "5-3-2": "1474996235279.png", "8-1":Pattern("1559113143860.png").similar(0.95), "8-2":Pattern("1567047814326.png").exact()}
farmingLabelDict = {"1-1":Pattern("1-1FarmLabel.PNG").similar(0.80), "4-2":Pattern("1583056649672.png").similar(0.90) , "6-1":Pattern("6-1FarmLabel.png").exact(), "6-2":Pattern("6-2FarmLabel.png").exact(), "7-1":Pattern("7-1FarmLabel.png").exact(), "7-3":Pattern("1597245252824.png").similar(0.90), "8-1":Pattern("8-1FarmLabel.PNG").similar(0.80), "8-2":Pattern("1567048024538.png").similar(0.80)}

farmingEra = farmingEraDict[farmingEraIndex]
farmingMap = farmingMapDict[farmingMapIndex]

# set node detection
if farmingMapIndex == "1-1" or farmingMapIndex == "4-2" or farmingMapIndex == "6-1" or farmingMapIndex == "6-2" or farmingMapIndex == "7-1" or farmingMapIndex == "7-3" or farmingMapIndex == "8-1" or farmingMapIndex == "8-2":
    nodeDetect = farmingLabelDict[farmingMapIndex]
else:
    nodeDetect = Pattern("1523954905742.png").similar(0.94)

# set formation
if formationType == "ATK":
    defaultFormation = "attackFormation.PNG"
elif formationType == "DEF" or formationType == "DEFADV":
    defaultFormation = "defaultFormation.png"
elif formationType == "SPD" or formationType == "SPDADV":
    defaultFormation = "speedFormation.PNG"
else:
     defaultFormation = "defendFormation.PNG"

if stopNodeIndex == "0":
    stopNode1 = None
    stopNode2 = None
else:
    stopNode1 = stopNodeDict[stopNodeIndex]
    if farmingMapIndex == "5-3":
        stopNode2 = stopNodeDict["5-3-2"]

#######################################################
#botting
while continuePlaying:
    log(".")

    #rule 7 - Step to next node
    if exists(nodeDetect, 0)!=None:
        log("steping")
        if exists("1559384076236.png", 15):
            log("tresure found")
            sleep(3)
            wait(Pattern("1523954905742.png").exact())
            continue
        
        log("enemy found")
        for i in range(0, 40):
            if exists("attackFormation.PNG", 0):
                chooseFormation()
                break
            elif exists("1560612593980.png", 0):
                log("kebii")
                App.focus("Chrome")
                focusWindow = App.focusedWindow()
                regionImage = capture(focusWindow)
                click(regionImage)
                continue
            elif exists("1614184220535.png", 0):
                mvpChecking()
                break
    
    #Rule 2 - Go to sorting
    elif exists("1559061307180.png", 0)!=None or exists("1559384310374.png", 0)!=None:
        log("sorting")
        goToSorting()
        continue

    #Rule 3 - Go to dungeon
    elif exists("1559058905319.png", 0)!=None:          
        log("dungeon")
        click("1559058905319.png")
        sleep(2)
        continue
    
    #Rule 4 - match farming location
    elif exists("1559058936255.png", 0):
        log("pick era and map")
        if exists(farmingEra)==None:
            if int(farmingEraIndex) >= 7:
                click(farmingEraDict["6"])
                doubleClick("1586258542083.png")
            else:
                click(farmingEraDict["3"])
                doubleClick("1586258565524.png")
        click(farmingEra)
        sleep(1)
        click(farmingMap)
        click("1575970354829.png")
        continue
    
    #Rule 6 - Ready to go farming!! 
    elif exists("1575970282642.png", 0): 
        if isEnergyCharging:
            goFarming()
        else:
            #If exhaust - rest for 5 min
            if farmingMapIndex != "4-2" and exists(Pattern("1560009393698.png").similar(0.80), 0)!=None:
                log("exhaust...")
                log("severe-switch captain")
                dragDrop("1560009393698.png", "1580603042266.png")
                sleep(2)
                click("1577529652016.png")
                goToHonmaru()
                log("sleep for 5 mins zZZ")
                sleep(300)
                continue
            if farmingMapIndex != "4-2" and isCheckExhaust and exists(Pattern("1560009578546.png").similar(0.80), 0)!=None:
                log("exhaust...")
                log("medium-switch captain")
                dragDrop("1560009578546.png", "1580603042266.png")
                sleep(2)
                isCheckExhaust = False
                continue

            isCheckExhaust = True
            #If any severe injury -> stop!
            if exists(Pattern("wound_severe.png").similar(0.81), 0)!=None:
                log("severe wound... end")
                click("1577529652016.png")
                goToHonmaru()
                if(isRepairRequired):
                    repair()
                else:
                    sys.exit("Mata ne arujisama!")
    
            #Stop when medium injury found if threshold is medium or small
            elif woundBeginThreshold <= 2 and exists("wound_medium.png", 0)!=None:
                log("medium wound... end")
                click("1577529652016.png")
                goToHonmaru()
                if(isRepairRequired):
                    repair()
                else:
                    sys.exit("Mata ne arujisama!")
    
            #Other cases -> go farming !!
            else:
                goFarming()
        continue

    #Rule 8 - Choose formation
    elif exists("defaultFormation.png", 0):
        chooseFormation()
        continue

    #Rule 21 - severe dialog
    elif exists("1559447913685.png", 0):
        log("severe wound... end")
        click("1577529729843.png")
        click("1559060085565.png")
        sleep(1)
        click("1577529715054.png")
        goToHonmaru()
        if(isRepairRequired):
            repair()
        else:
            sys.exit("Mata ne arujisama!")

    #Rule 10 - Show exp conclusion page
    elif exists("1614184220535.png", 0) and exists("1559060027510.png", 0)==None:
        mvpChecking()
        continue

    #Rule 11 - Choose to continue or go home
    elif exists("1559060027510.png", 0):
        checkGoHome()
        continue

    #Rule 14 - Expedite completed
    elif exists("1559385851648.png"):
        log("expedite is back")
        click("1559385851648.png")
        continue

    #Rule 18 - House keeping complete
    elif exists(Pattern("1597327999585.png").similar(0.80), 0):
        log("house keeping finished")
        if(isScreenshotRequired):
            takeScreenShot('workFinish')
        houseBtn = find(Pattern("1597327999585.png").similar(0.80))
        upperBtn = houseBtn.above(200)
        click(upperBtn)
        continue
    #Rule 13 - Get new sword
    elif exists("1580607759445.png",0) != None or exists("1580612814445.png", 0) != None:
        receiveNewMember()
        continue

    #Rule 20 - finish fighting
    elif exists("1466376360649.png", 0):
        log("finished fighting")
        click("1466376360649.png")
        continue
