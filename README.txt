----------------------------------------------------------------------------
# Tourabot # version 0.0.1/beta??
--------------------------------------------------------------------
**What is Tourabot**
	Tourabot is a bot for Touken Ranbu game written from a 
	laziness of nerd saniwa. The script is written in python.

**Requirement**
	1. JRE (preferably version 7 or 8 latest)
	2. SikuliX (available in https://launchpad.net/sikuli/+download)

**How to run with GUI**
	1. Start SikuliX GUI (sikuli.exe)
	2. Click File > Open
	   Direct to touraBot.sikuli folder
	3. Click Run button

**How to run with CMD**
	1. Start cmd.exe
	2. Direct to the folder where runsikulix.cmd locates
	3. Run command;
		runsikulix -r <path to touraBot.sikuli folder>

**Bot Configuration**
	Adjust following variable to configure the farming map
		- farmingEra: the era to be farm e.g. 5
		- farmingMap: the map in the era e.g. 5-4 
		- stopNode1 : a node you want the bot to go back 
			      to honmaru, usually is a node before
			      boss node
			      None if want to walk through boss
		- enemy     : the picture used to detect monster
			      face-left if the flow of map is left to right
			      face-right if the flow of map is right to left
			      enemy if there is both flow (not recommended)
	The farmingEra, farmingMap, and stopNode1 use the value from 
	dictionaries. The farmingEraDict is completed but the rest is not.
	Please ensure that the dictionaries of map and stopNode1 on wanted 
	era are added.
	To add the map or stopNode1, open the GUI or just use some tool to 
	capture the map/node picture. Remember, the captured node must contain
	the troop flag otherwise the bot will back to honmaru immediately.

**Remark**
	- The game window must be opened before the bot is run.
	- The game frame must remain in an original size.
	- Sikuli run a mouse to interact with GUI element. Hence,
	  you cannot play other games while running it. 
	  (Though, you can see anime :v)
	- Don't think there is anything else. Enjoy!!