:: Folder location of SikuliX - suggest that it should be under drive D directly
SET sikulixDir=D:\SikuliX

:: Folder location of touran bot - can be anywhere - suggest under drive D
SET touraBotDir=D:\ToukenBot\Dev\touraBot.sikuli

@ECHO OFF
ECHO.
ECHO Running Toubot...
ECHO.
CD /D %sikulixDir%
CALL runsikulix -r %touraBotDir% --args 5-4
ECHO Toubot is stopped...
PAUSE
CLS
EXIT